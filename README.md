# Git Secret

```sh
# Init git secret
git secret init

# Add user
git secret tell chitchaic@gmail.com

# Add secret files
git secret add .secrets

# Hide (encrypt secret) and delete plain file
git secret hide -d

# Decrypt
git secret reveal

```
